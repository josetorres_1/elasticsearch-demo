const elasticsearch = require("elasticsearch")
const clientES = new elasticsearch.Client({
    host: process.env.ELASTIC
})

const buildResponse = (status, event, errors, code) => {
    return {
        status: status,
        result: event,
        errors: errors,
        code: code
    }
}

module.exports.handler = async (event) => {
    let data = Object.assign({}, event)
    let result
    try {
        let searchParams = {
            index: data.type,
            body: {
                query: {
                    match_phrase_prefix: {
                        "name": data.object.name
                    }
                }
            }
        }
        result = await clientES.search(searchParams)
        if (result.hits.hits.length > 0) result = result.hits.hits
        else result = ["No records found"]
        return buildResponse("SUCCESS", result, [], 200)
    } catch (error) {
        console.trace(error.message)
        return buildResponse("FAIL", [], [error.message], 500)
    }
}
